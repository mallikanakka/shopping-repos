package com.shoppingapp.main;

import java.util.Scanner;

import com.shoppingapp.exception.IdNotFoundException;
import com.shoppingapp.model.Product;
import com.shoppingapp.service.IOrderService;
import com.shoppingapp.service.IProductService;
import com.shoppingapp.service.OrderServiceImpl;
import com.shoppingapp.service.ProductServiceImpl;

public class Admin {

	public static void main(String[] args) {
		IProductService productService = new ProductServiceImpl();
		IOrderService orderService=new OrderServiceImpl();
		Product product = null;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter 1) To Add a product");
		System.out.println("Enter 2) To Update a product");
		System.out.println("Enter 3) To Delete a product ");
		System.out.println("Enter 4) To See all the orders");
		System.out.println("Enter 5) To Exit");
	
		int choice = scanner.nextInt();

		switch (choice) {
		case 1:
			System.out.println("Enter product name");
			String productName = scanner.next();
			System.out.println("Enter price");
			double price = scanner.nextDouble();
			System.out.println("Enter category");
			String category = scanner.next();
			System.out.println("Enter brand");
			String brand = scanner.next();
			product = new Product(productName, price, category, brand);
			productService.addProduct(product);
			System.out.println("Added Successfuuly");
			break;
		case 2:
			System.out.println("Enter product id to update");
			int id = scanner.nextInt();
			System.out.println("Enter price to update");
			int updatePrice = scanner.nextInt();
			try {
				productService.updateProduct(id, updatePrice);
				System.out.println("Update Successfuuly");
			} catch (IdNotFoundException e) {

				e.printStackTrace();
			}
			break;
		case 3:
			System.out.println("Enter id to delete a product");
			int deleteId = scanner.nextInt();
			try {
				productService.deleteProduct(deleteId);
				System.out.println("Deleted Successfuuly");
			} catch (IdNotFoundException e) {
				e.printStackTrace();
			}
			break;
		case 4:
			System.out.println("These are all the orders");
			orderService.showAllordersForAdmin().forEach(System.out::println);
			break;
		case 5:
			System.exit(0);
		}

		scanner.close();
	}

}
