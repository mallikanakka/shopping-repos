
package com.shoppingapp.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.shoppingapp.exception.CategoryNotFoundException;
import com.shoppingapp.exception.ProductNotFoundException;
import com.shoppingapp.exception.UserNotFoundException;
import com.shoppingapp.model.User;
import com.shoppingapp.model.Cart;
import com.shoppingapp.model.Product;
import com.shoppingapp.service.CartServiceImpl;
import com.shoppingapp.service.ICartService;
import com.shoppingapp.service.IOrderService;
import com.shoppingapp.service.IProductService;
import com.shoppingapp.service.IUserService;
import com.shoppingapp.service.OrderServiceImpl;
import com.shoppingapp.service.ProductServiceImpl;
import com.shoppingapp.service.UserServiceImpl;

public class Client {

	public static void main(String[] args) {
		IUserService userService = new UserServiceImpl();
		IProductService productService = new ProductServiceImpl();
		IOrderService orderService = new OrderServiceImpl();
		ICartService cartService = new CartServiceImpl();
		User user = null;
		int userId = 0;
		boolean iterate = false;
		Scanner scanner = new Scanner(System.in);
		System.out.println();
		System.out.println("Hello welcome to smart shopping");
		System.out.println();
		System.out.println("Enter 1) To Login ");
		System.out.println("Enter 2) To Register");
		System.out.println("Enter 3) To Exit");
		System.out.println();
		int choice = scanner.nextInt();
		switch (choice) {
		case 1:
			try {
				System.out.println("Enter your email");
				String loginEmail = scanner.next();
				System.out.println("Enter your password");
				String loginPassword = scanner.next();
				user = userService.login(loginEmail, loginPassword);
				userId = userService.findUserId(loginEmail, loginPassword);

				if (user == null) {
					System.out.println("User not foud");

				} else {
					iterate = true;
					System.out.println("Welcome " + loginEmail + "....");
					System.out.println();
				}

			} catch (UserNotFoundException e) {

				System.out.println(e.getMessage());
			}

			break;

		case 2:
			System.out.println("Enter your name");
			String name = scanner.next();
			System.out.println("Enter your email");
			String email = scanner.next();
			System.out.println("set password");
			String password = scanner.next();
			System.out.println("Enter your mobile number");
			long mobile = scanner.nextLong();
			System.out.println("Enter your city");
			String city = scanner.next();
			if (email.endsWith("@gmail.com")) {
				user = new User(name, email, password, mobile, city);
				userService.registerUser(user);

				System.out.println("Registered Successfully");
			} else {
				System.out.println("Registration failed enter proper Email");
			}

			break;
		case 3:
			System.out.println("Thank you visit again");
			System.exit(10);
			break;
		default:
			System.out.println("Choose the proper option");
			break;

		}

		/* to iterate multiple times in switch case */

		List<Cart> cartList = new ArrayList<>();
		cartList = cartService.showCart(userId);
		while (iterate) {
			System.out.println("Enter 1) To see the Products");
			System.out.println("Enter 2) To see and add products to Cart");
			System.out.println("Enter 3) To order");
			System.out.println("Enter 4) To Exit");
			System.out.println();
			int shoppingChoice = scanner.nextInt();
			switch (shoppingChoice) {
			case 1:
				System.out.println("Find the products by ENTERING");
				System.out.println(" a) For all products");
				System.out.println(" b) For branded");
				System.out.println(" c) By category ");
				System.out.println(" n) Product name");
				System.out.println(" p) Price based ");
				System.out.println(" t) Brand,category,wanted price");
				String productChoice = scanner.next();

				switch (productChoice) {
				case "a":
					productService.getAllProducts().forEach(System.out::println);
					System.out.println();
					break;
				case "n":
					System.out.println("Enter product name to get similar products ");
					String name = scanner.next();
					try {
						productService.getByProductStarting(name).forEach(System.out::println);
					} catch (ProductNotFoundException e) {

						System.out.println(e.getMessage());
					}
					System.out.println();
					break;

				case "p":
					System.out.println("Enter first price");
					int firstPrice = scanner.nextInt();
					System.out.println("Enter end price");
					int endtPrice = scanner.nextInt();

					try {
						productService.getProductsBetweenPrice(firstPrice, endtPrice).forEach(System.out::println);
						System.out.println();
					} catch (ProductNotFoundException e) {

						System.out.println(e.getMessage());
					}

					break;

				case "b":
					System.out.println("Enter the brand name");
					String brand = scanner.next();
					try {
						productService.getByBrand(brand).forEach(System.out::println);
						System.out.println();
					} catch (ProductNotFoundException e) {

						System.out.println(e.getMessage());
					}

					break;

				case "c":
					System.out.println("Enter the category");
					String category = scanner.next();
					try {
						productService.getByCategory(category).forEach(System.out::println);
						System.out.println();
					} catch (CategoryNotFoundException e) {

						System.out.println(e.getMessage());
					}

					break;
				case "t":
					System.out.println("Enter the brand");
					String productBrand = scanner.next();
					System.out.println("Enter the category");
					String productCategory = scanner.next();
					System.out.println("Enter the price");
					double productPrice = scanner.nextDouble();

					try {
						productService.getByBrandAndCategoryWithLessPrice(productBrand, productCategory, productPrice)
								.forEach(System.out::println);
					} catch (ProductNotFoundException e) {

						System.out.println(e.getMessage());
					}

					break;

				default:
					System.out.println("Choose the proper option");
					break;

				}
				break;

			case 2:
				System.out.println("Enter a) To add a product to cart from products");
				System.out.println("      s) To see the cart");
				System.out.println("      r) To remove product from cart");
				System.out.println("      u) To update quantity of a product");
				System.out.println("      c) To calculate Cart Bill");
				String cartChoice = scanner.next();
				switch (cartChoice) {
				case "a":
					System.out.println("How many products you want to add");
					int addCount = scanner.nextInt();
					for (int i = 1; i < addCount + 1; i++) {
						System.out.println("Enter product" + i + " name");
						String productName = scanner.next();
						System.out.println("Enter product" + i + " brand");
						String productBrand = scanner.next();
						System.out.println("Enter product" + i + " quantity");
						int productQuantity = scanner.nextInt();
						int cartId = userId;
						try {
							Product product=productService.checkProductAvailability(productName, productBrand);
							if (product!=null) {
								Cart cart = new Cart(cartId, productName, productBrand, productQuantity, userId);
								cartService.addCart(cart);
							}
							else {
								System.out.println("Product not found to add to Cart");
							}
						
						} catch (ProductNotFoundException e) {
							System.out.println(e.getMessage());
						}
						

					}
					

					break;
				case "s":
					cartService.showCart(userId).forEach(System.out::println);
					System.out.println();
					break;
				case "r":
					System.out.println("Enter product name");
					String removeProduct = scanner.next();
					System.out.println("Enter brand");
					String removeBrand = scanner.next();
					try {
						cartService.removeProductFromCart(removeProduct, removeBrand, userId);
						System.out.println("Product Removed from Cart");
						System.out.println();
					} catch (ProductNotFoundException e) {

						System.out.println(e.getMessage());
					}
					break;
				case "c":
					int cartId = userId;
					double total = cartService.calculateBill(cartId);
					System.out.println("Here is your cart bill");
					System.out.println(total);
					System.out.println();
					break;

				case "u":
					System.out.println("Enter product name");
					String updateProduct = scanner.next();
					System.out.println("Enter brand");
					String updateBrand = scanner.next();
					System.out.println("Enter quantity");
					int updateQuantity = scanner.nextInt();
					try {
						cartService.updateProductQuantity(updateQuantity, updateProduct, updateBrand, userId);
						System.out.println("Updated Successfully");
						System.out.println();
					} catch (ProductNotFoundException e) {

						System.out.println(e.getMessage());
					}

					break;

				default:
					System.out.println("Choose the proper option");
					System.out.println();
					break;
				}

				break;
			case 3:
				int cartId = userId;
				System.out.println("Enter a) To order your cart now ");
				System.out.println("Enter b) To see all your orders");
				System.out.println("Enter c) To calculateBill");

				String orderChoice = scanner.next();
				switch (orderChoice) {
				case "a":
					if (cartList.isEmpty()) {
						System.out.println("No items in your cart to order");
						System.out.println();
					} else {
						orderService.addOrder(cartId);
						cartService.deleteCart(cartId);
						System.out.println("Ordered Successfully");
						System.out.println();
					}

					break;
				case "b":
					try {
						orderService.showAllorders(userId).forEach(System.out::println);
						System.out.println();
					} catch (UserNotFoundException e) {

						System.out.println(e.getMessage());
					}

					break;
				case "c":
					double total = cartService.calculateBill(cartId);
					System.out.println(total);
					break;
				default:
					System.out.println("Choose the proper option");
					System.out.println();
					break;

				}
				break;
			case 4:
				System.out.println("Thank you visit again");
				System.out.println();
				System.exit(10);
				break;
			default:
				System.out.println("Choose the proper option");
				System.out.println();
				break;

			}

		}
		scanner.close();
	}

}
