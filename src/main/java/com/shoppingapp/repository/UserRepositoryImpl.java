package com.shoppingapp.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.shoppingapp.exception.UserNotFoundException;
import com.shoppingapp.model.User;

public class UserRepositoryImpl implements IUserRepository {

	static Connection connection;

	@Override
	public void registerUser(User user) {

		PreparedStatement statement = null;
		try {
			connection = ModelDAO.openConnection();
			statement = connection.prepareStatement(Queries.USERINSERTQUERY);
			statement.setString(1, user.getUsername());
			statement.setString(2, user.getEmail());
			statement.setString(3, user.getPassword());
			statement.setLong(4, user.getMobile());
			statement.setString(5, user.getCity());
			boolean result = statement.execute();
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}
	}

	@Override
	public User login(String email, String password) throws UserNotFoundException {

		PreparedStatement statement = null;
		User user = null;
		try {
			connection = ModelDAO.openConnection();
			statement = connection.prepareStatement(Queries.LOGINUSERQUERY);
			statement.setString(1, email);
			statement.setString(2, password);
			ResultSet resultSet = null;
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				int userId1 = resultSet.getInt("userid");
				String useremail = resultSet.getString("email");
				String userPassword = resultSet.getString("password");
				user = new User(userId1, useremail, userPassword);
			}

			if (user==null) {
				throw new UserNotFoundException("User not found "+"\n" +"Please enter valid mail and password (or) Register if you are not regestered yet");
			
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}
		return user;
	}

	@Override
	public int findUserId(String email, String password) throws UserNotFoundException {
		
		PreparedStatement statement = null;

		User user = null;
		connection = ModelDAO.openConnection();
		try {
			statement = connection.prepareStatement(Queries.FINDUSERID);
			statement.setString(1, email);
			statement.setString(2, password);
			ResultSet resultSet = statement.executeQuery();
			;
			while (resultSet.next()) {
				int userId = resultSet.getInt("userid");
				user = new User(userId);
			}
			if (user==null) {
				throw new UserNotFoundException("User not found");

			}

			
		} catch (SQLException e) {
			
			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}

		return user.getUserId();
	}
}
