
/* mallika
 version 0.1*/

package com.shoppingapp.repository;

import com.shoppingapp.exception.UserNotFoundException;
import com.shoppingapp.model.User;

public interface IUserRepository {
	
	void registerUser(User user);
	User login(String email, String password) throws UserNotFoundException;
	int findUserId(String email, String password) throws UserNotFoundException;

}
