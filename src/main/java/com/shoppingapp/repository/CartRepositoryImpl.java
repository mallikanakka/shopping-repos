package com.shoppingapp.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.shoppingapp.exception.IdNotFoundException;
import com.shoppingapp.exception.ProductNotFoundException;
import com.shoppingapp.model.Cart;

public class CartRepositoryImpl implements ICartRepository {
	static Connection connection;

	@Override
	public void addCart(Cart cart) {

		PreparedStatement statement = null;
		connection = ModelDAO.openConnection();

		try {
			statement = connection.prepareStatement(Queries.CARTINSERTQUERY);
			statement.setInt(1, cart.getCartId());
			statement.setString(2, cart.getProductName());
			statement.setString(3, cart.getBrand());
			statement.setInt(4, cart.getQuantity());
			statement.setString(5, cart.getProductName());
			statement.setString(6, cart.getBrand());
			statement.setInt(7, cart.getUserId());
			statement.setDouble(8, cart.getQuantity());
			statement.setString(9, cart.getProductName());
			statement.setString(10, cart.getBrand());
			
			statement.execute();

		}  catch (SQLException e) {
			System.out.println(e.getMessage());
		}finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}
	}

	@Override
	public List<Cart> showCart(int userId) {
		List<Cart> cartList = new ArrayList<>();
		PreparedStatement statement = null;
		connection = ModelDAO.openConnection();
		try {
			statement = connection.prepareStatement(Queries.SELECTCARTQUERY);
			statement.setInt(1, userId);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				int cartId = resultSet.getInt("cartid");
				String productName = resultSet.getString("productname");
				String productBrand = resultSet.getString("brand");
				int productQuantity = resultSet.getInt("quantity");
				double price = resultSet.getDouble("price");
				double total = resultSet.getDouble("total");
				Cart cart = new Cart(cartId, productName, productBrand, productQuantity, price, total);
				cartList.add(cart);
			}

		}  catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}
		return cartList;

	}

	@Override
	public void removeProductFromCart(String productName, String brand, int userId) throws ProductNotFoundException {

		PreparedStatement statement = null;
		try {
			connection = ModelDAO.openConnection();
			statement = connection.prepareStatement(Queries.CARTPRODUCTDELETEQUERY);
			statement.setString(1, productName);
			statement.setString(2, brand);
			statement.setInt(3, userId);

			int count = statement.executeUpdate();
			if (count == 0) {
				throw new ProductNotFoundException("Product not found to remove");
			}
		}  catch (SQLException e) {
			System.out.println(e.getMessage());
		}finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}

	}

	@Override
	public void updateProductQuantity(int quantity, String productName, String brand, int userId)
			throws ProductNotFoundException {

		PreparedStatement statement = null;
		try {
			connection = ModelDAO.openConnection();
			statement = connection.prepareStatement(Queries.CARTPRODUCTUPDATEQUERY);
			statement.setInt(1, quantity);
			statement.setString(2, productName);
			statement.setString(3, brand);
			statement.setInt(4, userId);

			int count = statement.executeUpdate();
			if (count == 0) {
				throw new ProductNotFoundException("Product not found to remove");
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}

	}

	@Override
	public double calculateBill(int cartId) {
		PreparedStatement statement = null;
		double totalAmount = 0;
		connection = ModelDAO.openConnection();
		try {
			statement = connection.prepareStatement(Queries.CALCULATEBILLOFCART);
			statement.setInt(1, cartId);
			ResultSet resultSet = null;
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				totalAmount = resultSet.getDouble(1);

			}

		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}


		return totalAmount;

	}

	@Override
	public void deleteCart(int cartId) {

		PreparedStatement statement = null;
		connection = ModelDAO.openConnection();
		try {
			statement = connection.prepareStatement(Queries.DELETECARTQUERY);
			statement.setInt(1, cartId);
			statement.execute();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}  finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}

	}

}
