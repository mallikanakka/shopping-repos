package com.shoppingapp.repository;

import java.util.List;

import com.shoppingapp.exception.IdNotFoundException;
import com.shoppingapp.exception.ProductNotFoundException;
import com.shoppingapp.model.Cart;

public interface ICartRepository {
	
	void addCart(Cart cart);
	void removeProductFromCart(String productName,String brand,int userId) throws ProductNotFoundException;
	List<Cart> showCart(int userId);
	void updateProductQuantity(int quantity,String productName,String brand,int userId) throws ProductNotFoundException;
	void deleteCart(int cartId);
	public double calculateBill(int cartId);

}
