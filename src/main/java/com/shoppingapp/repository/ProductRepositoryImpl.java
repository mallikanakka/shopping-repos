package com.shoppingapp.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.shoppingapp.exception.CategoryNotFoundException;
import com.shoppingapp.exception.IdNotFoundException;
import com.shoppingapp.exception.ProductNotFoundException;
import com.shoppingapp.model.Product;

public class ProductRepositoryImpl implements IProductRepository {

	static Connection connection;

	@Override
	public void addProduct(Product product) {

		PreparedStatement statement = null;
		try {
			connection = ModelDAO.openConnection();
			statement = connection.prepareStatement(Queries.PRODUCTINSERTQUERY);
			statement.setString(1, product.getProductName());
			statement.setDouble(2, product.getPrice());
			statement.setString(3, product.getCategory());
			statement.setString(4, product.getBrand());
			statement.execute();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}
	}

	@Override
	public void updateProduct(int productId, double price) throws IdNotFoundException {

		PreparedStatement statement = null;
		try {
			connection = ModelDAO.openConnection();
			statement = connection.prepareStatement(Queries.PRODUCTUPDATEQUERY);
			statement.setDouble(1, price);
			statement.setInt(2, productId);
			int count = statement.executeUpdate();
			if (count == 0) {
				throw new IdNotFoundException("Product not found to update");
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}
	}

	@Override
	public void deleteProduct(int productId) throws IdNotFoundException {

		PreparedStatement statement = null;
		try {
			connection = ModelDAO.openConnection();
			statement = connection.prepareStatement(Queries.PRODUCTDELETEQUERY);
			statement.setInt(1, productId);
			int count = statement.executeUpdate();
			if (count == 0) {
				throw new IdNotFoundException("product id not found to delete");

			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}
	}

	@Override
	public List<Product> findAllProducts() {

		PreparedStatement statement = null;
		connection = ModelDAO.openConnection();
		List<Product> productList = new ArrayList<>();
		try {
			statement = connection.prepareStatement(Queries.PRODUCTSELECTALLQUERY);
			ResultSet resultSet = null;
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				String productname = resultSet.getString("productname");
				Double price = resultSet.getDouble("price");
				String category = resultSet.getString("category");
				String brand = resultSet.getString("brand");
				int productId = resultSet.getInt("productid");
				Product product = new Product(productId, productname, price, category, brand);
				productList.add(product);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}
		return productList;
	}

	@Override
	public List<Product> findByProductStarting(String productName) throws ProductNotFoundException {

		PreparedStatement statement = null;
		connection = ModelDAO.openConnection();
		ResultSet resultSet = null;
		List<Product> productList = new ArrayList<>();
		try {
			statement = connection.prepareStatement(Queries.GETBYPRODUCTSTARTSWITHQUERY);
			statement.setString(1, "%" + productName + "%");
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				String productname = resultSet.getString("productname");
				Double price = resultSet.getDouble("price");
				String category = resultSet.getString("category");
				String brand = resultSet.getString("brand");
				int productId = resultSet.getInt("productid");
				Product product = new Product(productId, productname, price, category, brand);
				productList.add(product);
			}
			if (productList.isEmpty()) {
				throw new ProductNotFoundException("Product not found with the name");
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}
		return productList;
	}

	@Override
	public List<Product> findByCategory(String category) throws CategoryNotFoundException {

		PreparedStatement statement = null;
		connection = ModelDAO.openConnection();
		ResultSet resultSet = null;
		List<Product> productList = new ArrayList<>();
		try {
			statement = connection.prepareStatement(Queries.GETBYCATEGORYQUERY);
			statement.setString(1, "%" + category + "%");
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				String productname = resultSet.getString("productname");
				Double price = resultSet.getDouble("price");
				String category1 = resultSet.getString("category");
				String brand = resultSet.getString("brand");
				int productId = resultSet.getInt("productid");
				Product product = new Product(productId, productname, price, category1, brand);
				productList.add(product);
			}
			if (productList.isEmpty()) {
				throw new CategoryNotFoundException("Product not found with this category");
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}
		return productList;
	}

	@Override
	public List<Product> findByBrand(String brand) throws ProductNotFoundException {

		PreparedStatement statement = null;
		connection = ModelDAO.openConnection();
		ResultSet resultSet = null;
		List<Product> productList = new ArrayList<>();
		try {
			statement = connection.prepareStatement(Queries.GETBYBRANDQUERY);
			statement.setString(1, "%" + brand + "%");
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				String productname = resultSet.getString("productname");
				Double price = resultSet.getDouble("price");
				String category = resultSet.getString("category");
				String brand1 = resultSet.getString("brand");
				int productId = resultSet.getInt("productid");
				Product product = new Product(productId, productname, price, category, brand1);
				productList.add(product);
			}
			if (productList.isEmpty()) {
				throw new ProductNotFoundException("Product not found with this brand");
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			ModelDAO.closeConnection();
		}
		return productList;
	}

	@Override
	public List<Product> findProductsBetweenPrice(double startPrice, double endPrice) throws ProductNotFoundException {

		PreparedStatement statement = null;
		connection = ModelDAO.openConnection();
		ResultSet resultSet = null;
		List<Product> productList = new ArrayList<>();
		try {
			statement = connection.prepareStatement(Queries.GETBYPRICEQUERY);
			statement.setDouble(1, startPrice);
			statement.setDouble(2, endPrice);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				String productname = resultSet.getString("productname");
				Double price = resultSet.getDouble("price");
				String category = resultSet.getString("category");
				String brand = resultSet.getString("brand");
				int productId = resultSet.getInt("productid");
				Product product = new Product(productId, productname, price, category, brand);
				productList.add(product);
			}
			if (productList.isEmpty()) {
				throw new ProductNotFoundException("Product not found between the price ");
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}
		return productList;
	}

	@Override
	public List<Product> findByBrandAndCategoryWithLessPrice(String brand, String category, double price)
			throws ProductNotFoundException {

		PreparedStatement statement = null;
		connection = ModelDAO.openConnection();
		ResultSet resultSet = null;
		List<Product> productList = new ArrayList<>();
		try {
			statement = connection.prepareStatement(Queries.GETBYBRANDCATEGORYANDPRICEQUERY);
			statement.setString(1, "%" + brand + "%");
			statement.setString(2, "%" + category + "%");
			statement.setDouble(3, price);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				String productname = resultSet.getString("productname");
				Double price1 = resultSet.getDouble("price");
				String category1 = resultSet.getString("category");
				String brand1 = resultSet.getString("brand");
				int productId = resultSet.getInt("productid");
				Product product = new Product(productId, productname, price1, category1, brand1);
				productList.add(product);
			}
			if (productList.isEmpty()) {
				throw new ProductNotFoundException("Product not found");
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}
		return productList;
	}

	@Override
	public Product checkProductAvailability(String name, String brand) throws ProductNotFoundException {
		Product product = null;
		PreparedStatement statement = null;
		connection = ModelDAO.openConnection();
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(Queries.GETPRODUCTQUERY);
			statement.setString(1, name);
			statement.setString(2, brand);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				String productname = resultSet.getString("productname");
				Double price1 = resultSet.getDouble("price");
				String category1 = resultSet.getString("category");
				String brand1 = resultSet.getString("brand");
				int productId = resultSet.getInt("productid");
				product = new Product(productId, productname, price1, category1, brand1);

			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}

		return product;

	}

}