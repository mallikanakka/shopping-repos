package com.shoppingapp.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.shoppingapp.exception.UserNotFoundException;
import com.shoppingapp.model.Cart;
import com.shoppingapp.model.Order;

public class OrderRepositoryImpl implements IOrderRepository {
	ICartRepository cartRepositoty = new CartRepositoryImpl();
	static Connection connection;

	@Override
	public void addOrder(int cartId) {

		List<Cart> listCart = cartRepositoty.showCart(cartId);
		String allProducts = "";
		String allBrands = "";
		String quantities = "";
		PreparedStatement statement = null;
		connection = ModelDAO.openConnection();
		for (Cart eachCart : listCart) {
			allProducts += eachCart.getProductName() + " : ";
			allBrands += eachCart.getBrand() + " : ";
			quantities += eachCart.getQuantity() + " : ";
		}

		try {
			statement = connection.prepareStatement(Queries.INSERTORDERQUERY);
			statement.setInt(1, cartId);
			statement.setString(2, allProducts);
			statement.setString(3, allBrands);
			statement.setString(4, quantities);
			statement.setInt(5, cartId);
			statement.setDouble(6, cartRepositoty.calculateBill(cartId));
			statement.execute();

		} catch (SQLException e) {

			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}

	}

	@Override
	public List<Order> showAllorders(int userId) throws UserNotFoundException {

		List<Order> orderList = new ArrayList<>();
		PreparedStatement statement = null;
		connection = ModelDAO.openConnection();
		try {
			statement = connection.prepareStatement(Queries.SELECTORDERQUERY);
			statement.setInt(1, userId);
			ResultSet resultSet = null;
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				int orderId = resultSet.getInt("orderid");
				int cartId = resultSet.getInt("cartid");
				String productNames = resultSet.getString("productnames");
				String brands = resultSet.getString("brands");
				String qunatities = resultSet.getString("quantities");
				int user = resultSet.getInt("userid");
				Date date = resultSet.getDate("date");
				double totalAmount = resultSet.getDouble("totalamount");
				Order order = new Order(orderId, cartId, productNames, brands, qunatities, user, (java.sql.Date) date,
						totalAmount);
				orderList.add(order);

			}
			if (orderList.isEmpty()) {
				throw new UserNotFoundException("this user didn,t order any");
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}

		return orderList;
	}
	
	
	@Override
	public List<Order> showAllordersForAdmin()  {

		List<Order> orderList = new ArrayList<>();
		PreparedStatement statement = null;
		connection = ModelDAO.openConnection();
		try {
			statement = connection.prepareStatement(Queries.SELECTALLORDERQUERY);
			ResultSet resultSet = null;
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				int orderId = resultSet.getInt("orderid");
				int cartId = resultSet.getInt("cartid");
				String productNames = resultSet.getString("productnames");
				String brands = resultSet.getString("brands");
				String qunatities = resultSet.getString("quantities");
				int user = resultSet.getInt("userid");
				Date date = resultSet.getDate("date");
				double totalAmount = resultSet.getDouble("totalamount");
				Order order = new Order(orderId, cartId, productNames, brands, qunatities, user, (java.sql.Date) date,
						totalAmount);
				orderList.add(order);

			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			ModelDAO.closeConnection();
		}

		return orderList;
	}
	
	
	
	
	
	
}
