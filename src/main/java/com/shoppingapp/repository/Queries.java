package com.shoppingapp.repository;

public class Queries {

	public static final String INSERTORDERQUERY = "insert into allorders(cartid,productnames,brands,quantities,userid,totalAmount) values(?,?,?,?,?,?)";
	public static final String GETPRICEQUERY = "select price from product where productname=? and brand=?";
	public static final String SELECTORDERQUERY = "select * from allorders where userid=?";
	public static final String SELECTALLORDERQUERY="select * from allorders";
	
	public static final String CARTINSERTQUERY = "insert into cart(cartid,productname,brand,quantity,price,userid,total) values(?,?,?,?,(select price from product where productname=? and brand=?) ,?,?*(select price from product where productname=?  and brand=?))";
	public static final String SELECTCARTQUERY = "select * from cart where cartid=?";
	public static final String CARTPRODUCTDELETEQUERY = "delete from cart where productname=? and brand=? and userid=?";
	public static final String CARTPRODUCTUPDATEQUERY = "update cart set quantity=? where productname=? and brand=? and userid=?";
	public static final String DELETECARTQUERY = "delete from cart where cartid=?";
	public static final String CALCULATEBILLOFCART="select sum(total) from cart where cartid=?";
	
	public static final String USERINSERTQUERY = "insert into users(username,email,password,mobile,city) values(?,?,?,?,?)";
	public static final String LOGINUSERQUERY = "select userid,email,password from users where email=? and password=?";
	public static final String FINDUSERID = "select userid from users where email=? and password=?";

	public static final String PRODUCTINSERTQUERY = "insert into product(productname,price,category,brand) values (?,?,?,?)";
	public static final String PRODUCTUPDATEQUERY = "update product set price=? where productid=?";
	public static final String PRODUCTDELETEQUERY = "delete from product where productid=?";
	public static final String PRODUCTSELECTALLQUERY = "SELECT * from product";
	public static final String GETBYPRODUCTSTARTSWITHQUERY = "SELECT * from product where productname like ?";
	public static final String GETBYCATEGORYQUERY = "SELECT * from product where category like ?";
	public static final String GETBYBRANDQUERY = "SELECT * from product where brand like ?";
	public static final String GETBYPRICEQUERY = "SELECT * from product where price between ? and ?";
	public static final String GETBYBRANDCATEGORYANDPRICEQUERY = "select * from product where brand like ? and category like ? and price<?";
	public static final String GETPRODUCTQUERY="select * from product where productname=? and brand=?";
	
	public static final String INSERTINTOCARTQUERY = "insert into cart(cartid,productid,quantity,productbill) values (?,?,?,?)";
	public static final String REMOVEPRODUCTQUERY = "delete from cart where productid=?";
	public static final String SHOWCART = "select * from cart";

}
