package com.shoppingapp.repository;

import java.util.List;

import com.shoppingapp.exception.CategoryNotFoundException;
import com.shoppingapp.exception.IdNotFoundException;
import com.shoppingapp.exception.ProductNotFoundException;
import com.shoppingapp.model.Product;

public interface IProductRepository {

	void addProduct(Product product);

	void updateProduct(int productId, double price) throws IdNotFoundException;

	void deleteProduct(int productId) throws IdNotFoundException;

	List<Product> findAllProducts();

	List<Product> findByProductStarting(String productName) throws ProductNotFoundException;

	List<Product> findByCategory(String category) throws CategoryNotFoundException;

	List<Product> findByBrand(String brand) throws ProductNotFoundException;

	List<Product> findByBrandAndCategoryWithLessPrice(String brand, String category, double price)
			throws ProductNotFoundException;

	List<Product> findProductsBetweenPrice(double startPrice, double endPrice) throws ProductNotFoundException;

	Product checkProductAvailability(String name, String brand) throws ProductNotFoundException;

	

}
