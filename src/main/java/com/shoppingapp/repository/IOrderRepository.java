package com.shoppingapp.repository;

import java.util.List;

import com.shoppingapp.exception.UserNotFoundException;
import com.shoppingapp.model.Order;

public interface IOrderRepository {
	public void addOrder(int cartId);
	public List<Order> showAllorders(int userId) throws UserNotFoundException;
	public List<Order> showAllordersForAdmin();
}
