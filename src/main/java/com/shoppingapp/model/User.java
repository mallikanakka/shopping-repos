package com.shoppingapp.model;

public class User {

	int userId;
	String username;
	String email;
	String password;
	long mobile;
	String city;

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the mobile
	 */
	public long getMobile() {
		return mobile;
	}

	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(long mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	public User() {

	}

	public User(int userId) {
		this.userId = userId;
	}

	public User(int userId, String email, String password) {
		this.userId = userId;
		this.email = email;
		this.password = password;
	}

	/**
	 * 
	 * @param username
	 * @param password
	 * @param mobile
	 */

	public User(String username, String password, long mobile) {

		this.username = username;
		this.password = password;
		this.mobile = mobile;
	}

	/**
	 * @param email
	 * @param password
	 * 
	 */

	public User(String email, String password) {

		this.email = email;
		this.password = password;
	}

	/**
	 * 
	 * @param userId
	 * @param username
	 * @param password
	 * @param mobile
	 */

	public User(String username, String email, String password, long mobile, String city) {

		this.username = username;
		this.email = email;
		this.password = password;
		this.mobile = mobile;
		this.city = city;
	}

	public User(int userId, String username, String email, String password, long mobile, String city) {

		this.userId = userId;
		this.username = username;
		this.email = email;
		this.password = password;
		this.mobile = mobile;
		this.city = city;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", username=" + username + ", email=" + email + ", password=" + password
				+ ", mobile=" + mobile + ", city=" + city + "]";
	}

}
