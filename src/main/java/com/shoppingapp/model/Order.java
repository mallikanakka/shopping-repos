
/* mallikaprodu
 version 0.1*/

package com.shoppingapp.model;

import java.sql.Date;

public class Order {

	int orderId;
	int cartId;
	String productNames;
	String brands;
	String quantities;
	int userId;
	Date date;
	double totalAmount;

	/**
	 * @param orderId
	 * @param cartId
	 * @param productName
	 * @param brand
	 * @param quantity
	 * @param userId
	 * @param date
	 * @param totalAmount
	 */
	public Order(int orderId, int cartId, String productNames, String brands, String quantities, int userId, Date date,
			double totalAmount) {
		super();
		this.orderId = orderId;
		this.cartId = cartId;
		this.productNames = productNames;
		this.brands = brands;
		this.quantities = quantities;
		this.userId = userId;
		this.date = date;
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the totalAmount
	 */
	public double getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount the totalAmount to set
	 */
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the orderId
	 */
	public int getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	/**
	 * @return the cartId
	 */
	public int getCartId() {
		return cartId;
	}

	/**
	 * @param cartId the cartId to set
	 */
	public void setCartId(int cartId) {
		this.cartId = cartId;
	}

	/**
	 * @return the productNames
	 */
	public String getProductNames() {
		return productNames;
	}

	/**
	 * @param productNames the productNames to set
	 */
	public void setProductNames(String productNames) {
		this.productNames = productNames;
	}

	/**
	 * @return the brands
	 */
	public String getBrands() {
		return brands;
	}

	/**
	 * @param brands the brands to set
	 */
	public void setBrands(String brands) {
		this.brands = brands;
	}

	/**
	 * @return the quantities
	 */
	public String getQuantities() {
		return quantities;
	}

	/**
	 * @param quantities the quantities to set
	 */
	public void setQuantities(String quantities) {
		this.quantities = quantities;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", cartId=" + cartId + ", productNames=" + productNames + ", brands="
				+ brands + ", quantities=" + quantities + ", userId=" + userId + ", date=" + date + ", totalAmount="
				+ totalAmount + "]";
	}

}
