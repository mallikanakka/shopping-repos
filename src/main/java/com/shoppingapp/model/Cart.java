package com.shoppingapp.model;

public class Cart {
	int cartId;
	String productName;
	String brand;
	int quantity;
	double price;
	int userId;
	double total;
	

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * @return the total
	 */
	public double getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(double total) {
		this.total = total;
	}

	/**
	 * @return the cartId
	 */
	public int getCartId() {
		return cartId;
	}

	/**
	 * @param cartId the cartId to set
	 */
	public void setCartId(int cartId) {
		this.cartId = cartId;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * @param brand the brand to set
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}


	/**
	 * @param cartId
	 * @param productName
	 * @param brand
	 * @param quantity
	 * @param userId
	 * 
	 */

	public Cart(int cartId, String productName, String brand, int quantity, int userId) {

		this.cartId = cartId;
		this.productName = productName;
		this.brand = brand;
		this.quantity = quantity;
		this.userId = userId;
		
		
	}

	

	/**
	 * @param cartId
	 * @param productName
	 * @param brand
	 * @param quantity
	 * @param price
	 * @param userId
	 * @param total
	 */
	public Cart(int cartId, String productName, String brand, int quantity, double price,  double total) {
		super();
		this.cartId = cartId;
		this.productName = productName;
		this.brand = brand;
		this.quantity = quantity;
		this.price = price;
		
		this.total = total;
	}

	/**
	 * @param productName
	 * @param brand
	 * @param quantity
	 */
	public Cart(String productName, String brand, int quantity) {
		super();
		this.productName = productName;
		this.brand = brand;
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Cart [cartId=" + cartId + ", productName=" + productName + ", brand=" + brand + ", quantity=" + quantity
				+ ", price=" + price  + ", total=" + total + "]";
	}

	

	

	

}
