package com.shoppingapp.model;

public class Product {
	int productId;
	String productName;
	double price;
	String category;
	String brand;
	String units;

	/**
	 * @return the productId
	 */
	public int getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(int productId) {
		this.productId = productId;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * @param brand the brand to set
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}

	/**
	 * @return the units
	 */
	public String getUnits() {
		return units;
	}

	/**
	 * @param units the units to set
	 */
	public void setUnits(String units) {
		this.units = units;
	}

	public Product(String units) {
		super();
		this.units = units;
	}

	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param productName
	 * @param price
	 * @param category
	 * @param brand
	 */
	public Product(String productName, double price, String category, String brand) {
		super();
		this.productName = productName;
		this.price = price;
		this.category = category;
		this.brand = brand;
	}

	/**
	 * @param productId
	 * @param productName
	 * @param price
	 * @param category
	 * @param brand
	 */

	public Product(int productId, String productName, double price, String category, String brand) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.price = price;
		this.category = category;
		this.brand = brand;
	}

	@Override
	public String toString() {
		return "Product [productName=" + productName + ", productId=" + productId + ", price=" + price + ", category="
				+ category + ", brand=" + brand + "]";
	}
}
