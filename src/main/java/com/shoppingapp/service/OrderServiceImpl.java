package com.shoppingapp.service;

import java.util.List;

import com.shoppingapp.exception.UserNotFoundException;
import com.shoppingapp.model.Order;
import com.shoppingapp.repository.IOrderRepository;
import com.shoppingapp.repository.OrderRepositoryImpl;

public class OrderServiceImpl implements IOrderService {
	IOrderRepository orderRepository = new OrderRepositoryImpl();

	@Override
	public void addOrder(int cartId) {
		orderRepository.addOrder(cartId);

	}

	/*
	 * @Override public double calculateBill(int cartId) { return
	 * orderRepository.calculateBill(cartId); }
	 */

	@Override
	public List<Order> showAllorders(int userId) throws UserNotFoundException {
		return orderRepository.showAllorders(userId);
	}

	@Override
	public List<Order> showAllordersForAdmin() {
		// TODO Auto-generated method stub
		return orderRepository.showAllordersForAdmin();
	}

}
