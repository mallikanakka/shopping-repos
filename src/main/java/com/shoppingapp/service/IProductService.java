package com.shoppingapp.service;

import java.util.List;

import com.shoppingapp.exception.CategoryNotFoundException;
import com.shoppingapp.exception.IdNotFoundException;
import com.shoppingapp.exception.ProductNotFoundException;
import com.shoppingapp.model.Product;

public interface IProductService {

	void addProduct(Product product);

	void updateProduct(int productId, double price) throws IdNotFoundException;

	void deleteProduct(int productId) throws IdNotFoundException;

	List<Product> getAllProducts();

	List<Product> getByProductStarting(String productName) throws ProductNotFoundException;

	List<Product> getByCategory(String category) throws CategoryNotFoundException;

	List<Product> getByBrand(String brand) throws ProductNotFoundException;

	List<Product> getProductsBetweenPrice(double startPrice, double endPrice) throws ProductNotFoundException;

	List<Product> getByBrandAndCategoryWithLessPrice(String brand, String category, double price)
			throws ProductNotFoundException;

	Product checkProductAvailability(String name, String brand) throws ProductNotFoundException;


}
