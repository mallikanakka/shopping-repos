
/* mallika
 version 0.1*/

package com.shoppingapp.service;

import java.util.List;

import com.shoppingapp.exception.UserNotFoundException;
import com.shoppingapp.model.Order;

public interface IOrderService {
	public void addOrder(int cartId);
	//public double calculateBill(int cartId);
	public List<Order> showAllorders(int userId) throws UserNotFoundException;
	public List<Order> showAllordersForAdmin();
}
