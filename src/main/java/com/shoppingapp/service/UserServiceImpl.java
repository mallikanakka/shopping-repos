package com.shoppingapp.service;

import com.shoppingapp.exception.UserNotFoundException;
import com.shoppingapp.model.User;
import com.shoppingapp.repository.IUserRepository;
import com.shoppingapp.repository.UserRepositoryImpl;

public class UserServiceImpl implements IUserService {

	IUserRepository userRepository = new UserRepositoryImpl();

	@Override
	public void registerUser(User user) {

		userRepository.registerUser(user);
	}

	@Override
	public User login(String email, String password) throws UserNotFoundException {

		return userRepository.login(email, password);
	}

	@Override
	public int findUserId(String email, String password) throws UserNotFoundException{

		return userRepository.findUserId(email, password);
	}
}
