package com.shoppingapp.service;

import java.util.List;
import java.util.stream.Collectors;

import com.shoppingapp.exception.CategoryNotFoundException;
import com.shoppingapp.exception.IdNotFoundException;
import com.shoppingapp.exception.ProductNotFoundException;
import com.shoppingapp.model.Product;
import com.shoppingapp.repository.IProductRepository;
import com.shoppingapp.repository.ProductRepositoryImpl;

public class ProductServiceImpl implements IProductService {

	IProductRepository productRepository = new ProductRepositoryImpl();

	public void addProduct(Product product) {

		productRepository.addProduct(product);
	}

	public void updateProduct(int productId, double price) throws IdNotFoundException {

		productRepository.updateProduct(productId, price);
	}

	public void deleteProduct(int productId) throws IdNotFoundException {

		productRepository.deleteProduct(productId);
	}

	public List<Product> getAllProducts() {

		return productRepository.findAllProducts().stream()
				.sorted((o1, o2) -> o1.getProductName().compareTo(o2.getProductName())).collect(Collectors.toList());
	}

	public List<Product> getByProductStarting(String productName) throws ProductNotFoundException {

		return productRepository.findByProductStarting(productName);
	}

	public List<Product> getByCategory(String category) throws CategoryNotFoundException {

		return productRepository.findByCategory(category);
	}

	public List<Product> getByBrand(String brand) throws ProductNotFoundException {

		return productRepository.findByBrand(brand);
	}

	public List<Product> getProductsBetweenPrice(double startPrice, double endPrice) throws ProductNotFoundException {

		return productRepository.findProductsBetweenPrice(startPrice, endPrice);
	}

	@Override
	public List<Product> getByBrandAndCategoryWithLessPrice(String brand, String category, double price)
			throws ProductNotFoundException {
		return productRepository.findByBrandAndCategoryWithLessPrice(brand, category, price);
	}

	@Override
	public Product checkProductAvailability(String name, String brand) throws ProductNotFoundException {
		// TODO Auto-generated method stub
		return productRepository.checkProductAvailability(name, brand);
	}
}
