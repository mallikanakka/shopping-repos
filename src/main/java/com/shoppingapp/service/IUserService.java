package com.shoppingapp.service;

import com.shoppingapp.exception.UserNotFoundException;
import com.shoppingapp.model.User;

public interface IUserService {

	void registerUser(User user);

	User login(String email, String password) throws UserNotFoundException;

	int findUserId(String email, String password) throws UserNotFoundException;
}
