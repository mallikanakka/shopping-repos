package com.shoppingapp.service;

import java.util.List;

import com.shoppingapp.exception.ProductNotFoundException;
import com.shoppingapp.model.Cart;
import com.shoppingapp.repository.CartRepositoryImpl;
import com.shoppingapp.repository.ICartRepository;

public class CartServiceImpl implements ICartService {
	ICartRepository cartRepository = new CartRepositoryImpl();

	@Override
	public void addCart(Cart cart) {
		cartRepository.addCart(cart);
	}

	@Override
	public void removeProductFromCart(String productName, String brand, int userId) throws ProductNotFoundException {
		cartRepository.removeProductFromCart(productName, brand, userId);

	}

	@Override
	public List<Cart> showCart(int userId) {
		return cartRepository.showCart(userId);

	}

	@Override
	public void updateProductQuantity(int quantity, String productName, String brand, int userId)
			throws ProductNotFoundException {
		cartRepository.updateProductQuantity(quantity, productName, brand, userId);

	}

	@Override
	public void deleteCart(int cartId) {
		cartRepository.deleteCart(cartId);

	}

	@Override
	public double calculateBill(int cartId) {
		// TODO Auto-generated method stub
		return cartRepository.calculateBill(cartId);
	}

}
